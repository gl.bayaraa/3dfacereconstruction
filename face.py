import cv2
import os
import numpy as np

face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
eye_cascade  = cv2.CascadeClassifier(cv2.data.haarcascades+'haarcascade_eye.xml')

# gray_image = cv2.cvtColor(original_image , cv2.COLOR_BGR2GRAY)
# faces = face_cascade.detectMultiScale(gray_image , 1.3 , 5)

# cv2.imshow("image" , faces)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

def detectFace (directory):
    faces = list()
    i = 0
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if filename.endswith(".jpg") or filename.endswith(".jpeg") or filename.endswith(".png"):
            print("Opening ")
            print(str(os.path.join(directory, filename)))
            face = processImage(str(os.path.join(directory, filename)), i)
            faces.append(face)
            i+=1
    return faces



def processImage(imagePath, index):
    haar_flag = 0
    image_scale = 1
    min_neighbor = 5
    minSize = (20, 20)
    haar_scale_factor = 1.3

    try: 
        image =  cv2.imread(imagePath)
        
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, haar_scale_factor, min_neighbor)    
        for (x, y, w, h) in faces:        
            crop = image[y: y+h , x: x+w]
            cv2.imwrite("D:/Python/3DMM/output/" + str(index) + ".png", crop)
            return crop
        

    except Exception as e:
        print ("Failed to read an image file | Corrupted image file %s" % e)